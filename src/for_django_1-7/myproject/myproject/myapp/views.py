# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from myproject.myapp.models import Document
from myproject.myapp.forms import DocumentForm

import httplib2
import html2text
import webbrowser
import os
import re
from django.conf import settings

'''
  Função para encontrar uma string entre dois substring
'''
def find_between( s, first, last ):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return ""

'''
   Função que prenche os dados para fazer os gráficos da V2 e V3
'''
def fill_version(content, graphic_1, graphic_2, vulnerabilities, i):
    cvssScore = find_between(content, "\))\n", "\n\nVector:")
    if (cvssScore == "LOW"):
        graphic_1[0] = graphic_1[0] + 1
    elif (cvssScore == "MEDIUM"):
        graphic_1[1] = graphic_1[1] + 1
    elif (cvssScore == "HIGH"):
        graphic_1[2] = graphic_1[2] + 1
    elif (cvssScore == "CRITICAL"):
        graphic_1[3] = graphic_1[3] + 1
    cvssV2ImpactScore = find_between(content, "Impact Subscore: ", "\n")
    cvssV2ExploitabilityScore = find_between(content, "Exploitability Subscore: ", "\n")
    graphic_2.append([vulnerabilities[i].rstrip(), cvssV2ImpactScore, cvssV2ExploitabilityScore])

'''
    Funcao que verifica se a versão é V2 ou V3
'''
def make_graphics(vulnerabilities):
    v2_graphic_2 = []
    v2_graphic_1 = [0]*4
     
    v3_graphic_2 = []
    v3_graphic_1 = [0]*4
    url_temp="https://web.nvd.nist.gov/view/vuln/detail?vulnId="
    for i in range(len(vulnerabilities)):
        url=url_temp+vulnerabilities[i]
        resp, content = httplib2.Http().request(url)
        text_file = open(os.path.join(settings.MEDIA_ROOT,"content.html"), "w")
        text_file.write(content)
        text_file.close()
        html = open(os.path.join(settings.MEDIA_ROOT,"content.html")).read()
        content = html2text.html2text(html)
        cvssV2BaseScore = find_between(content, "CVSS v2 Base Score:\n[", "](https:")
        cvssV3BaseScore = find_between(content, "CVSS v3 Base Score:\n[", "](https:")
        if (cvssV2BaseScore!=''):
            fill_version(content, v2_graphic_1, v2_graphic_2, vulnerabilities, i)
        else:
            fill_version(content, v3_graphic_1, v3_graphic_2, vulnerabilities, i)

    return v2_graphic_1,v2_graphic_2,v3_graphic_1,v3_graphic_2
         
'''
    Funcao que define o formulario para fazer os testes da prova. A entrada 
    da aplicacao deve ser um arquivo onde suas linhas contém as vulnerabilidades
'''
def list(request):
    # Handle file upload
    graphic_1 = None
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            newdoc = Document(docfile = request.FILES['docfile'])
            newdoc.save()
            documents = Document.objects.all()
            fname = documents[len(documents)-1].docfile.name
            with open(os.path.join(settings.MEDIA_ROOT, fname), 'rb') as vulners:
                vulnerabilities = vulners.readlines()
            v2_graphic_1,v2_graphic_2,v3_graphic_1,v3_graphic_2 = make_graphics(vulnerabilities)
            return render_to_response('myapp/graphics.html', {'v2_graphic_1':v2_graphic_1,'v2_graphic_2':v2_graphic_2,'v3_graphic_1':v3_graphic_1,'v3_graphic_2':v3_graphic_2}, context_instance=RequestContext(request))

    else:
        form = DocumentForm() # A empty, unbound form

    return render_to_response(
        'myapp/list.html',
        {'form': form},
        context_instance=RequestContext(request)
    )
